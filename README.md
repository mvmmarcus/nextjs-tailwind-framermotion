# 🔗 Aplicação SPA com Next.js & TailwindCSS & Framer Motion

## Simples aplicação utilizando Nextjs , Tailwind CSS e Framermotion para praticar os conhecimentos com essas tecnologias

###

<div style="width: 100%; align-items: center; text-align: center;" >
<img align="center" alt="GitHub Pipenv locked Python version" src="https://img.shields.io/github/pipenv/locked/python-version/metabolize/rq-dashboard-on-heroku"/>
<img align="center" alt="typescript" src="https://img.shields.io/npm/types/typescript"/>

</div>

![](demo-next-app.gif)

###

<p align="center">
 <a href="#-tecnologias">Tecnologias</a> • 
 <a href="#-instalacao">Instalação</a> •
 <a href="#-contribuicoes">Contribuições</a> 
</p>

###

### 🛠 Tecnologias

<div id="tecnologias" >
As seguintes ferramentas foram usadas no desenvolvimento da aplicação:

- [Nextsjs](https://nextjs.org/)
- [Tailwind](https://tailwindcss.com/)
- [Framer Motion](https://www.framer.com/motion/)
</div>

###

### 🚀 Como executar o projeto

<div id="instalacao" >

### Clone este repositório

$ git clone git@gitlab.com:mvmmarcus/nextjs-tailwind-framermotion.git

### Acesse a pasta do projeto no terminal/cmd

$ cd/nextjs-tailwind-framermotion

### Instale as dependências

$ yarn ou npm install

### Execute a aplicação

$ yarn dev ou npm run dev

</div>

###

## 👨‍💻 Desenvolvedor

Um agradecimento especial ao pessoal da <a href="https://rocketseat.com.br/" >Rocketseat</a>, que disponibilizaram um conteúdo fantástico durante o evento "Do While 2020" 👏

<table id="contribuicoes" >
  <tr>
    <td align="center"><a href="https://gitlab.com/mvmmarcus"><img style="border-radius: 50%;" src="https://gitlab.com/uploads/-/system/user/avatar/6195744/avatar.png?width=400" width="100px;" alt=""/><br /><sub><b>Marcus Vinícius</b></sub></a><br /><a href="https://gitlab.com/mvmmarcus" title="Marcus Vinicius">👨‍🚀</a></td>
  </tr>
</table>

Desenvolvido com ❤️ por <a href="https://gitlab.com/mvmmarcus">Marcus Vinícius</a>
